#!/bin/bash

set -eux

default=bruno-nl-main
remote=bruno-nl
assignee=margebot-bruno


create_dummy_mr() {
    # RANDOM needed for creating MRs in //
    #ts=$(date +%s)$(echo $((RANDOM % 1000)))
    ts=$(date +%s)
    branch_name="onurb@${ts}"


    git fetch "$remote" "${default}"
    git checkout -b "$branch_name" "$remote"/"${default}"
    echo "dummy" > "src/$ts" # create a new file
    git add "src/$ts"
    git commit "src/$ts" -m "Dummy commit: $ts"

    # https://docs.gitlab.com/ee/user/project/push_options.html
    git push "$remote" "$branch_name" \
        --push-option merge_request.create \
        --push-option merge_request.title="Dummy MR ${ts}" \
        --push-option merge_request.target="${default}" \
        --push-option merge_request.assign="${assignee}"

    # delete branch
    git checkout "$default"
    git branch -D "$branch_name"
}


create_dummy_mr_behind() {
    # RANDOM needed for creating MRs in //
    #ts=$(date +%s)$(echo $((RANDOM % 1000)))
    ts=$(date +%s)
    branch_name="onurb@${ts}"


    git checkout "${default}"
    git checkout -b "$branch_name"
    echo "dummy" > "src/$ts" # create a new file
    git add "src/$ts"
    git commit "src/$ts" -m "Dummy commit: $ts"

    # https://docs.gitlab.com/ee/user/project/push_options.html
    git push "$remote" "$branch_name" \
        --push-option merge_request.create \
        --push-option merge_request.title="Dummy MR ${ts}" \
        --push-option merge_request.target="${default}" \
        --push-option merge_request.assign="${assignee}"

    # delete branch
    git checkout "$default"
    git branch -D "$branch_name"
}


clean_branches(){
    # List all local branches that start with "foobar"

    #prefix="$1"
    branches=$(git branch | grep "^\s*$1")

    # Loop through each branch and delete it
    for branch in $branches; do
        git branch -D "$branch"
    done

}



create_dummy_mrs() {
    for ((i=1; i <= "$1"; i++)); do
        create_dummy_mr
    done
}


# https://unix.stackexchange.com/questions/103920/parallelize-a-bash-for-loop
# https://unix.stackexchange.com/a/103921
# parallel creation of MRs: does not work as is - locks
# "Another git process seems to be running in this repository"
create_dummy_mrs_par() {
    ssh-add-fix

    for ((i=1; i <= "$1"; i++)); do
        create_dummy_mr& # NOTE: probably need to restore use of RANDOM in create_dummy_mr, otherwise clash in branch named
    done
    wait
}

# one-liner
# ssh-add-fix ; for i in {1..5}; do create_dummy_mr & done ; wait

# BUG: terminal output
# From gitlab.com:nomadic-labs/bruno-tezos
#  * branch                  bruno-nl-main -> FETCH_HEAD
# From gitlab.com:nomadic-labs/bruno-tezos
#  * branch                  bruno-nl-main -> FETCH_HEAD
# fatal: Unable to create '/home/bruno/git/tezos/.git/index.lock': File exists.

# Another git process seems to be running in this repository, e.g.
# an editor opened by 'git commit'. Please make sure all processes
# are terminated then try again. If it still fails, a git process
# may have crashed in this repository earlier:
# remove the file manually to continue.
# fatal: Unable to create '/home/bruno/git/tezos/.git/index.lock': File exists.

# Another git process seems to be running in this repository, e.g.
# an editor opened by 'git commit'. Please make sure all processes
# are terminated then try again. If it still fails, a git process
# may have crashed in this repository earlier:
# remove the file manually to continue.
# error: pathspec 'src/1716387506969' did not match any file(s) known to git
# M	new_mr.sh
# branch 'onurb@1716387506661' set up to track 'bruno-nl/bruno-nl-main'.
# error: src refspec onurb@1716387506969 does not match any
# Switched to a new branch 'onurb@1716387506661'
# error: failed to push some refs to 'gitlab.com:nomadic-labs/bruno-tezos.git'
# fatal: Unable to create '/home/bruno/git/tezos/.git/index.lock': File exists.
