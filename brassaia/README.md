# Brassaia Libraries

## Overview

Brassaia (which name comes from the 'Brassaia Actinophyllia' tree, also called the "octopus tree") is a library based on Irmin (see [irmin/README.md](../irmin/README.md)) that is used in the Octez suite.

Brassaia is a simplification and an optimization of Irmin for the specific needs of the Octez suite. It is not intended to be a general-purpose library.
It is currently a work in progress and should not be used in production. Octez will continue to use Irmin until Brassaia is ready.

Brassaia License is the same as Irmin's: ISC.
